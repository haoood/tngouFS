
CREATE DATABASE IF NOT EXISTS `tnfs` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `tnfs`;



CREATE TABLE IF NOT EXISTS `tnfs_gallery` (
`id` smallint(5) unsigned NOT NULL,
  `galleryclass` tinyint(3) unsigned NOT NULL COMMENT '图片分类',
  `title` varchar(64) NOT NULL COMMENT '标题',
  `img` varchar(128) NOT NULL COMMENT '图库封面',
  `count` mediumint(8) unsigned NOT NULL COMMENT '访问数',
  `rcount` smallint(5) unsigned NOT NULL COMMENT '回复数',
  `fcount` smallint(5) unsigned NOT NULL COMMENT '收藏数',
  `size` tinyint(3) unsigned NOT NULL COMMENT '图片多少张',
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '时间'
) ENGINE=Aria  DEFAULT CHARSET=utf8  COMMENT='图库，' AUTO_INCREMENT=1 ;



DROP TABLE IF EXISTS `tnfs_galleryclass`;
CREATE TABLE IF NOT EXISTS `tnfs_galleryclass` (
`id` tinyint(3) unsigned NOT NULL COMMENT 'ID 编号',
  `name` varchar(8) NOT NULL COMMENT '名称',
  `title` varchar(32) NOT NULL,
  `keywords` varchar(64) NOT NULL,
  `description` varchar(128) NOT NULL,
  `seq` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=Aria  DEFAULT CHARSET=utf8  TRANSACTIONAL=0 COMMENT='图片分类' AUTO_INCREMENT=1 ;


DROP TABLE IF EXISTS `tnfs_picture`;
CREATE TABLE IF NOT EXISTS `tnfs_picture` (
`id` mediumint(8) unsigned NOT NULL,
  `gallery` mediumint(8) unsigned NOT NULL COMMENT '图库',
  `src` varchar(128) NOT NULL COMMENT '图片地址'
) ENGINE=Aria  DEFAULT CHARSET=utf8  AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `tnfs_user` (
`id` smallint(6) NOT NULL,
  `account` varchar(32) NOT NULL COMMENT '账户',
  `password` char(32) NOT NULL COMMENT '密码'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;




ALTER TABLE `tnfs_gallery`
 ADD PRIMARY KEY (`id`);


ALTER TABLE `tnfs_galleryclass`
 ADD PRIMARY KEY (`id`);


ALTER TABLE `tnfs_picture`
 ADD PRIMARY KEY (`id`);


ALTER TABLE `tnfs_gallery`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

ALTER TABLE `tnfs_galleryclass`
MODIFY `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID 编号',AUTO_INCREMENT=1;

ALTER TABLE `tnfs_picture`
MODIFY `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;


INSERT INTO `tnfs_user` (`id`, `account`, `password`) VALUES
(1, 'tngou', '90a19eb43ef6f37ebc7cfdd49fdb46df');


ALTER TABLE `tnfs_user`
 ADD PRIMARY KEY (`id`);



ALTER TABLE `tnfs_user`
MODIFY `id` smallint(6) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;


