package net.tngou.tnfs.service;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import net.tngou.tnfs.cache.CacheEngine;
import net.tngou.tnfs.cache.EhCacheEngine;
import net.tngou.tnfs.jdbc.Inflector;
import net.tngou.tnfs.jdbc.OrderType;
import net.tngou.tnfs.pojo.Galleryclass;
import net.tngou.tnfs.pojo.POJO;
import net.tngou.tnfs.util.PageUtil;



public class BaseService {
	protected static CacheEngine cacheEngine =new  EhCacheEngine().getInstance();
	public PageUtil getPage(int page,int size,Class<? extends POJO> pojoClass) {
		String fullyQualifiedName = StringUtils.capitalize(Inflector.getInstance().fullyQualifiedName(pojoClass));
		Serializable key="getPage_p"+page+"_s"+size;
		PageUtil pageUtil = (PageUtil) cacheEngine.get(fullyQualifiedName, key);
		if(pageUtil==null)
		{
			try {
				POJO pojo = pojoClass.newInstance();
				pageUtil = new PageUtil(pojo.list(page, size),page,size,pojo.totalCount());
				cacheEngine.add(fullyQualifiedName, key, pageUtil);
			} catch (InstantiationException | IllegalAccessException e) {
			
				e.printStackTrace();
			}
		}
		return pageUtil;
		
	}
	public PageUtil getPage(int page,int size,String filter,Class<? extends POJO> pojoClass) {
		String fullyQualifiedName = StringUtils.capitalize(Inflector.getInstance().fullyQualifiedName(pojoClass));
		Serializable key="getPage_p"+page+"_s"+size+"_f"+filter;
		PageUtil pageUtil = (PageUtil) cacheEngine.get(fullyQualifiedName, key);
		if(pageUtil==null)
		{
			try {
				POJO pojo = pojoClass.newInstance();
				pageUtil = new PageUtil(pojo.list(filter,page, size),page,size,pojo.totalCount(filter));
				cacheEngine.add(fullyQualifiedName, key, pageUtil);
			} catch (InstantiationException | IllegalAccessException e) {
			
				e.printStackTrace();
			}
		}
		
		return pageUtil;
		
	}
	
	
	public List<?> getList(Class<? extends POJO> pojoClass) {
		String fullyQualifiedName = StringUtils.capitalize(Inflector.getInstance().fullyQualifiedName(pojoClass));
		Serializable key="getList";
		List<?> list= (List<?>) cacheEngine.get(fullyQualifiedName, key);
		if(list==null)
		{
			try {
				POJO pojo = pojoClass.newInstance();
				list=pojo.list();
				cacheEngine.add(fullyQualifiedName, key, list);
			} catch (InstantiationException | IllegalAccessException e) {
			
				e.printStackTrace();
			}
		}
		return list;
		
	}
	
	
	public List<?> getList(String type,OrderType orderType,Class<? extends POJO> pojoClass) {
		String fullyQualifiedName = StringUtils.capitalize(Inflector.getInstance().fullyQualifiedName(pojoClass));
		Serializable key="getList_t"+type+"_o"+orderType.toString();
		List<?> list= (List<?>) cacheEngine.get(fullyQualifiedName, key);
		if(list==null)
		{
			try {
				POJO pojo = pojoClass.newInstance();
				list=pojo.list(type, orderType);
				cacheEngine.add(fullyQualifiedName, key, list);
			} catch (InstantiationException | IllegalAccessException e) {
			
				e.printStackTrace();
			}
		}
		return list;
		
	}
	
	public List<?> getList(String filter,Class<? extends POJO> pojoClass) {
		String fullyQualifiedName = StringUtils.capitalize(Inflector.getInstance().fullyQualifiedName(pojoClass));
		Serializable key="getList_f"+filter;
		List<?> list= (List<?>) cacheEngine.get(fullyQualifiedName, key);
		if(list==null)
		{
			try {
				POJO pojo = pojoClass.newInstance();
				list=pojo.list(filter, "id", OrderType.ASC);
				cacheEngine.add(fullyQualifiedName, key, list);
			} catch (InstantiationException | IllegalAccessException e) {
			
				e.printStackTrace();
			}
		}
		
		return list;
		
	}
	
	
	/**
	 * 
	* @Description: 清除缓存
	 */
	public  void removeCache(String fullyQualifiedName) 
	{
		cacheEngine.remove(fullyQualifiedName);
	}
}
