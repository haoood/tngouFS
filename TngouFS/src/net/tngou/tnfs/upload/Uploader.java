package net.tngou.tnfs.upload;

import java.util.Map;

import net.tngou.tnfs.define.State;
import net.tngou.tnfs.http.HttpRequest;


public class Uploader {
	private HttpRequest request = null;
	private Map<String, Object> conf = null;

	public Uploader(HttpRequest request, Map<String, Object> conf) {
		this.request = request;
		this.conf = conf;
	}

	public final State doExec() {
		String filedName = (String) this.conf.get("fieldName");
		State state = null;

		if ("true".equals(this.conf.get("isBase64"))) {
			state = Base64Uploader.save(this.request.getParameter(filedName),
					this.conf);
		} else {
			state = BinaryUploader.save(this.request, this.conf);
		}

		return state;
	}
}
