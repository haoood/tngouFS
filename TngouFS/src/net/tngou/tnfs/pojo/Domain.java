package net.tngou.tnfs.pojo;

import java.io.Serializable;


public class Domain implements Serializable
{
	
	

	private static final long serialVersionUID = 1L;
	private String base;
	private String manage;
	private String imgurl;
	public String getBase() {
		return base;
	}
	public void setBase(String base) {
		this.base = base;
	}
	public String getManage() {
		return manage;
	}
	public void setManage(String manage) {
		this.manage = manage;
	}
	public String getImgurl() {
		return imgurl;
	}
	public void setImgurl(String imgurl) {
		this.imgurl = imgurl;
	}
	
	
	
	
}
