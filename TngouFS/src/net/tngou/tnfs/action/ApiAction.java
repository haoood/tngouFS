package net.tngou.tnfs.action;

import java.util.List;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import net.tngou.tnfs.enums.CacheEnum;
import net.tngou.tnfs.enums.CountEnum;
import net.tngou.tnfs.enums.TTypeEnum;
import net.tngou.tnfs.jdbc.OrderType;
import net.tngou.tnfs.pojo.Ask;
import net.tngou.tnfs.pojo.Gallery;
import net.tngou.tnfs.pojo.Galleryclass;
import net.tngou.tnfs.pojo.Picture;
import net.tngou.tnfs.quartz.CacheUtil;
import net.tngou.tnfs.service.GalleryService;
import net.tngou.tnfs.util.PageUtil;



public class ApiAction extends BaseAction {
	private Ask ask=null;
	
	
	/*
	 * 分类
	 */
	public void classify() {
		ask=getAsk(Ask.class);
		@SuppressWarnings("unchecked")
		List<Galleryclass> list = (List<Galleryclass>) baseService.getList("seq", OrderType.ASC, Galleryclass.class);
		printJson(this.toJsonP(list));
	}
	
	
	
	/*
	 * 取得图库
	 */
	public void list()
	{
		ask=getAsk(Ask.class);
		String filter="1=1";
		if(ask.getId()!=0) filter = "galleryclass="+ask.getId();
		PageUtil page = baseService.getPage(ask.getPage(), ask.getRows(), filter , Gallery.class);
		int total = page.getTotal();
		 @SuppressWarnings("unchecked")
		List<Gallery> list = (List<Gallery>) page.getList();	 	
		 String json=JSON.toJSONString(list);
		printJson(this.toJsonP(ask.getCallback(), json,total));
	}
	
	
	/*
	 * 取得最新的
	 */
	public void news() {
		ask=getAsk(Ask.class);
		GalleryService galleryService = new GalleryService();
		PageUtil page=galleryService.getNewsPage(ask.getId(),ask.getRows(),ask.getClassify());	
		int total = page.getTotal();
		 @SuppressWarnings("unchecked")
		List<Gallery> list = (List<Gallery>) page.getList();	 	
		 String json=JSON.toJSONString(list);
		printJson(this.toJsonP(ask.getCallback(), json,total));
	}
	
	
	/*
	 * 展示图库
	 */
	public void show() {
		ask=getAsk(Ask.class);
		Gallery gallery = new Gallery().get(ask.getId());
		if(gallery==null){run_false("不存储在图片库！");return;}
		CacheUtil.getInstance(CacheEnum.VisitEh, CountEnum.count).Add(gallery.getId(), TTypeEnum.tnfs_gallery);
		root.put("gallery", gallery);
		String filter="gallery="+gallery.getId(); 
		List<?> list =  baseService.getList(filter,Picture.class);
//		JSONObject jObject = JSONObject.parseObject(JSON.toJSONString(gallery));
		JSONObject jObject=(JSONObject) JSONObject.toJSON(gallery);
//		 String json=JSON.toJSONString(list);
		
		 jObject.put("list", list);
		 jObject.put("url", "http://www.tngou.net/tnfs/show/"+gallery.getId());
		printJson(this.toJsonP(jObject));
	}
	
	
	/**
	 * 这里返回的数据主要有JSonp了Json两种处理方式
	 */
	public String toJsonP(String callback,String json)
	{
		if(callback!=null)
		{
			json=callback+"("+json+")";
		}
		return json;
	}
	
	public String toJsonP( JSONObject jsonObject)
	{
		jsonObject.put("status", true);	
		return toJsonP(ask.getCallback(), jsonObject.toJSONString());
	}
	public String toJsonP(String callback,String json,int total)
	{
		if(callback==null)
		{
			json="{\"status\":"+true+", \"total\":"+total+",\"tngou\":"+json+"}";
		}else
		{
			json=callback+"({\"status\":"+true+",\"total\":"+total+", \"tngou\":"+json+"})";
		}
		return json;
	}
	
	public String toJsonP(JSONArray array) {
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("tngou", array);
		return toJsonP(jsonObject);
	}


	 /**
	  * 404页面
	  */
	 public void run_false(String message) {
			
		 JSONObject jsonObject = new JSONObject();
		 jsonObject.put("status", false);
		 jsonObject.put("msg", message);		
		printJson(this.toJsonP(jsonObject));
	}
	 
	 public String toJsonP(List<?> list)
	{
			JSONArray array = (JSONArray) JSONObject.toJSON(list);
			return toJsonP(array);
	}

}
