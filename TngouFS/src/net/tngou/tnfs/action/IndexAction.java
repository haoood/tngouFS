package net.tngou.tnfs.action;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;

import net.tngou.tnfs.jdbc.OrderType;
import net.tngou.tnfs.pojo.Gallery;
import net.tngou.tnfs.pojo.Galleryclass;
import net.tngou.tnfs.util.PageUtil;


public class IndexAction extends BaseAction {

	

	
	@Override
	public void execute() throws ServletException, IOException {
		int page= request.getParameter("p")==null?1:Integer.parseInt(request.getParameter("p"));
		List<?> galleryclasses = baseService.getList("seq",OrderType.ASC,Galleryclass.class);	
		root.put("galleryclasses", galleryclasses);
		
		String[] params = request.getParams();
		String title="天狗阅图-美女 性感 诱惑 日韩_打造时尚开放美图";
		String keywords="性感美女 ,韩日美女 ,丝袜美腿 ,美女照片 ,美女写真 ,清纯美女, 性感车模 ,  美图,照片,图片,美女图片,车模图片,性感图片,火辣图片,真空,街拍,KDS宽带山,宽带山生活";
		String description="天狗阅图，开创开放阅图模式 ，主要包括 性感美女 ,韩日美女 ,丝袜美腿 ,美女照片 ,美女写真 ,清纯美女, 性感车模 ,性感图片,火辣图片,真空,街拍美图欣赏";
		
		if(params==null)
		{
			PageUtil pageUtil = baseService.getPage(page, SIZE,  Gallery.class);
			root.put("page", pageUtil);	
		}else
		{
			Galleryclass galleryclass = new Galleryclass().get(Long.parseLong(params[0]));
			
			String filter="1=1";
			if(galleryclass!=null)filter="galleryclass="+galleryclass.getId();
			PageUtil pageUtil = baseService.getPage(page, SIZE, filter, Gallery.class);
			root.put("page", pageUtil);	
			root.put("galleryclass", galleryclass);
			title=galleryclass.getTitle();
			keywords=galleryclass.getKeywords();
			description=galleryclass.getDescription();
		}
		
		if(page>1) title=title+"_第"+page+"页";
		 root.put("title", title);
		 root.put("keywords", keywords);
		 root.put("description", description);
		printFreemarker("default/index.ftl", root);
	}
}
