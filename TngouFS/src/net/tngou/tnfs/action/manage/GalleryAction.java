package net.tngou.tnfs.action.manage;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import net.tngou.tnfs.jdbc.Inflector;
import net.tngou.tnfs.jdbc.OrderType;
import net.tngou.tnfs.pojo.Gallery;
import net.tngou.tnfs.pojo.Galleryclass;
import net.tngou.tnfs.pojo.POJO;
import net.tngou.tnfs.pojo.Picture;
import net.tngou.tnfs.service.BaseService;
import net.tngou.tnfs.util.ImgUtil;
import net.tngou.tnfs.util.PageUtil;

/**
 * 
* @Description: 图库
* @author tngou@tngou.net (www.tngou.net)
* @date 2015年7月9日 下午6:11:50
*
 */
public class GalleryAction extends IndexAction {

	
	/**
	 * 图库列表
	 */
	public void list() {
		int page= request.getParameter("p")==null?1:Integer.parseInt(request.getParameter("p"));
		String[] params = request.getParams();
		String filter = "galleryclass="+params[0];
		Galleryclass galleryclass = new Galleryclass().get(Long.parseLong(params[0]));
		root.put("galleryclass", galleryclass);
		PageUtil pageUtil = baseService.getPage(page, 20, filter , Gallery.class);
		root.put("page", pageUtil);
		
		printFreemarker("manage/gallerys.ftl", root);
		
	}
	
	/**
	 * 显示图库
	 */
	public void show() {
		String[] params = request.getParams();
		Gallery gallery = new Gallery().get(Long.parseLong(params[0]));
		root.put("gallery", gallery);
		String filter="gallery="+gallery.getId(); 
		List<?> list =  baseService.getList(filter,Picture.class);
		root.put("list", list);
		Galleryclass galleryclass = new Galleryclass().get(gallery.getGalleryclass());
		root.put("galleryclass", galleryclass);
		
		printFreemarker("manage/gallery.ftl", root);
	}
	
	
	
	/*
	 * 编辑图库
	 */
	public void edit() {
		
		if(!request.isSubmit())
		{
			String id = request.getParameter("id");
			
			Gallery gallery = new Gallery().get(Long.parseLong(id));
			root.put("gallery", gallery);
			String filter="gallery="+gallery.getId(); 
			Picture bean = new Picture();
			
			List<?> list =  bean.list(filter, "id", OrderType.DESC);
			root.put("list", list);
			Galleryclass galleryclass = new Galleryclass().get(gallery.getGalleryclass());
			root.put("galleryclass", galleryclass);
			printFreemarker("manage/gallery_edit.ftl", root);
		}else
		{
			Gallery gallery = getAsk(Gallery.class);
			
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("title", gallery.getTitle());
			map.put("galleryclass", gallery.getGalleryclass());
			map.put("size", gallery.getSize());
			gallery.update(map , gallery.getId());
			//清除缓存
			sendRedirect(getDomain().getManage()+"/gallery/list/"+gallery.getGalleryclass());
		}
		
	}
	
	
	
	public void delete() {
		
		String id = request.getParameter("id");
		Gallery gallery = new Gallery().get(Long.parseLong(id));
		
		String filter="gallery="+id;
		Picture bean = new Picture();	
		List<Picture> list =  (List<Picture>) bean.list(filter, "id", OrderType.DESC);
		list.forEach(e->{
			ImgUtil.Drop(e.getSrc());
			
		});
		
		String rurl = getDomain().getManage()+"/gallery/list/"+gallery.getGalleryclass();
		gallery.delete();
		String fullyQualifiedName = StringUtils.capitalize(Inflector.getInstance().fullyQualifiedName(Gallery.class));
	    baseService.removeCache(fullyQualifiedName);
		printHtml(rurl);
		
	}
	
	
	
	public void deletepicture() {
		
		String id = request.getParameter("id");
		Picture picture = new Picture().get(Long.parseLong(id));
		
		String src = picture.getSrc();
		ImgUtil.Drop(src);
		
		int pid = picture.getGallery();
		Gallery gallery = new Gallery();
		gallery=gallery.get(pid);
		gallery.setSize(gallery.getSize()-1);
		gallery.update();
		picture.delete();
		printHtml(id);
		
	}
	
	
}
