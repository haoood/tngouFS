package net.tngou.tnfs.action.manage;

import java.io.IOException;

import javax.servlet.ServletException;

import org.apache.commons.lang3.StringUtils;

import net.tngou.tnfs.cache.CacheEngine;
import net.tngou.tnfs.cache.EhCacheEngine;
import net.tngou.tnfs.jdbc.Inflector;
import net.tngou.tnfs.pojo.Galleryclass;

public class ClassifyAction extends IndexAction {

	
	@Override
	public void execute() throws ServletException, IOException {
		printFreemarker("manage/classify.ftl", root);
	}
	
	
	/*
	 * 编辑分类
	 */
	public void edit() {
		
		if(!request.isSubmit())
		{
			String id = request.getParameter("id");
			Galleryclass galleryclass = new Galleryclass().get(Long.parseLong(id));
			root.put("galleryclass", galleryclass);
			printFreemarker("manage/classify_edit.ftl", root);
		}else
		{
			Galleryclass galleryclass = getAsk(Galleryclass.class);
			galleryclass.update();
			//清除缓存
			String fullyQualifiedName = StringUtils.capitalize(Inflector.getInstance().fullyQualifiedName(Galleryclass.class));
		    baseService.removeCache(fullyQualifiedName);
			sendRedirect(getDomain().getManage()+"/classify");
		}
		
	}
	
	/*
	 * 添加分类
	 */
	public void add() {
		
		if(!request.isSubmit())
		{	
			printFreemarker("manage/classify_add.ftl", root);
		}else
		{
			Galleryclass galleryclass = getAsk(Galleryclass.class);
			galleryclass.save();
			//清除缓存
			String fullyQualifiedName = StringUtils.capitalize(Inflector.getInstance().fullyQualifiedName(Galleryclass.class));
		    baseService.removeCache(fullyQualifiedName);
			sendRedirect(getDomain().getManage()+"/classify");
		}
		
	}
	
	
}
