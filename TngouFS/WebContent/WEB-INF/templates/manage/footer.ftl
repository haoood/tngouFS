</div>

<a href="#" class="am-show-sm-only admin-menu" data-am-offcanvas="{target: '#admin-offcanvas'}">
  <span class="am-icon-btn am-icon-th-list"></span>
</a>

<footer>
  <hr>
  <p class="am-padding-left">© 2015 Tngou.NET. </p>
</footer>

<!--[if lt IE 9]>

<script src="http://cdn.staticfile.org/modernizr/2.8.3/modernizr.js"></script>
<script src="${Domain.base}/common/amazeui/js/amazeui.ie8polyfill.min.js"></script>
<![endif]-->

<!--[if (gte IE 9)|!(IE)]><!-->
<!--<![endif]-->
<script src="${Domain.base}/common/amazeui/js/amazeui.min.js"></script>
<script src="${Domain.base}/common/amazeui/js/app.js"></script>
</body>
</html>